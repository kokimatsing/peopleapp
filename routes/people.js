var express = require('express');
var router = express.Router();
var People = require('../models/people');

/*
 * Return a list of people
 */
router.get('/', function(req, res){ 
	People.find({},{},{}, function(err, people){
		if(!err){
			res.json(people);
		} else {
			res.json(err);
		}
	});
});


/*
 * Return person by id
 */
router.get('/:personid', function(req, res){   
	People.findOne({_id:req.params.personid},{},{}, function(err, people){
		if(!err){
			res.json(people);
		} else {
			res.json(err);
		}
	});
});

/*
 * Create a new person
 */
router.post('/', function(req, res){
	People.create({
		first_name: req.body.first_name,
		last_name: req.body.last_name,
		contact_number: req.body.contact_number
	},function(err, people){
		if(!err){
			res.json(people);
		} else {
			res.json('error');
		}
	})
});

/*
 * Edit person details by id
 */
router.put('/:peopleid', function(req, res){ 
	People.findByIdAndUpdate(req.params.peopleid, { 
		first_name: req.body.first_name,
		last_name: req.body.last_name,
		contact_number: req.body.contact_number
	}, function(err, people){
		if(!err){
			res.json(people);
		}else{
			console.log(err);
			res.json('error');
		};
	});
});

/*
 * Delete person by id
 */
router.delete('/:peopleid', function(req, res){
	People.remove({
		'_id': req.params.peopleid
	},function(err, people){
		if(!err){
			res.json('success');
		} else {
			res.json('error');
		}
	})
});

module.exports = router;