module.exports = function (app) {
    app.use('/', require('./home')); 			 	// homepage to serve the angularjs app 
    app.use('/api/people', require('./people'));    // api route for People
};