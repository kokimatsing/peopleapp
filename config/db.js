module.exports = function(mongoose) {  
	var dbURI = 'mongodb://localhost:27017/peopledb';
	mongoose.connect(dbURI, { useNewUrlParser: true });

	mongoose.connection.on('connect', function(){
		console.log('Mongoose connected on ' + dbURI);
	});

	mongoose.connection.on('error', function(err){
		console.log('Mongoose connection : ' + err);
	}); 
}