var mongoose = require('mongoose');

var peopleSchema = new mongoose.Schema({
	id: Number,
	first_name: String,
	last_name: String,
	contact_number: String,
});

module.exports = mongoose.model('People', peopleSchema);