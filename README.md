# PeopleApp
PeopleApp with Mongo, Expressjs, Angularjs, Nodejs

Clone this repo and install dependencies by running ```npm install```

Configure you mongo in the ```/config/db.js```

Refer to the ```/public/js/app.js``` file for the angular **controllers**, **services**, and **config**

Check the ```/public/templates``` directory for the templates

The main layout is the ```/views/index.pug``` file

Refer to the ```/routes/people.js``` for the backend REST api

Run ```node index``` and browse to ```localhost:4000```
 