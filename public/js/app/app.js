/*
 * Create an angular App
 */
angular.module('peopleApp', ['ngRoute', 'ngResource'])

	/*
	 * Assign the controller and template for each URLs
	 */
	.config(function($routeProvider, $httpProvider){
		$routeProvider
			.when('/', {
				controller: 'peopleCtrl',
				templateUrl: '../js/templates/people.html'
			})
			.when('/people/:personid', {
				controller: 'detailCtrl',
				templateUrl: '../js/templates/detail.html'
			})
			.when('/people/:personid/edit', {
				controller: 'editCtrl',
				templateUrl: '../js/templates/form.html'
			})
			.otherwise({
				redirectTo:'/'
			});
	})

	/*
	 * Create a service that will manage CRUD request to server
	 */
	.factory('People', ['$resource', function($resource){
			return $resource('/api/people/:id',{id:'@_id'},{
				'index': { method: 'GET', isArray: true },
      			'show': { method: 'GET', isArray: false },
      			'create': { method: 'POST' },
      			'update': { method:'PUT' },
      			'remove': {method:'DELETE'}
			});
		}])

	/*
	 * Create controller for the homepage
	 */
	.controller('peopleCtrl', function($scope, People){
		// use the People service to fetch all the list of people
		People.index(function(people){
			$scope.people = people; 
		});
	})

	/*
	 * Create controller for the detail page
	 */
	.controller('detailCtrl', function($scope, $routeParams, $location, People){
		// get the person id from the get parameter
		var personId = $routeParams.personid;

		// use the personId on Person service to fetch the person details
		People.show({id:personId}, function(person){
			$scope.personDetail = person;
		});

		// submit a delete request using the People service whicl delete button was clicked
		$scope.deletePerson = function(personId){
			People.remove({id:personId}, function(result){
				if('error' != result){
					$location.url('/refresh');
				}
			});
		};
	})

	/*
	 * Create controller for the add form 
	 */
	.controller('addCtrl', function($scope, $location, People){
		// get the data on form and send it to server using the People service
		$scope.submitPerson = function(){
			// check if all fields are filled
			if($scope.personFirstName && $scope.personLastName && $scope.personContact) { 
				personData = {
					first_name: $scope.personFirstName,
					last_name: $scope.personLastName,
					contact_number: $scope.personContact
				};

				People.create(personData, function(data){
					if('error' != data){
						$scope.personFirstName = '';
						$scope.personLastName = '';
						$scope.personContact = '';
						$location.url('/refresh');
					}
				})
			} else {
				$scope.formError = 'All fields are required.'
			}
		}
	})

	/*
	 * Create controller for the edit page
	 */
	.controller('editCtrl', function($scope, $routeParams, $location, People){
		var personId = $routeParams.personid;
		
		// get the person details to populate the edit form
		People.show({id:personId}, function(people){
			$scope.personDetail = people;
		});

		// redirect to person details when "back" button was clicked
		$scope.cancelEdit = function(){
			$location.url('/people/'+personId);
		}

		// save the changes to the person details
		$scope.savePerson = function(){ 
			// check if all fields are filled
			if($scope.personDetail.first_name && $scope.personDetail.last_name && $scope.personDetail.contact_number) { 
				People.update({id:personId}, $scope.personDetail, function(result){
					// display message if the operation is success or error
					if('error' != result){
						$scope.formSuccess = 'Changes has been saved';
						$scope.formError = '';
					}else{
						$scope.formError = 'Unable to save changes';
						$scope.formSuccess = '';
					}
				});
			} else {
				$scope.formError = 'All fields are required.'
				$scope.formSuccess = '';
			}

			return false;
		}
	})